
import 'bootstrap/dist/css/bootstrap.min.css'
import img1 from './assets/images/1.jpg'
import img2 from './assets/images/2.jpg'
import img3 from './assets/images/3.jpg'
import img4 from './assets/images/4.jpg'
import hawaiiPizza from './assets/images/hawaiian.jpg'
import seafoodPizza from './assets/images/seafood.jpg'
import baconPizza from './assets/images/bacon.jpg'
import { FaLinkedin,FaTwitter,FaPinterest,FaSnapchat,FaInstagram,FaFacebook,FaArrowUp} from 'react-icons/fa'

import './App.css'
function App() {
  return (
   <div>
    <div className="container-fluid web-navbar-menu">
        <div className="row">
            <div className="col-12">
                <nav className="navbar fixed-top navbar-expand-lg navbar-light">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav nav-fill w-100">
                            <li className="nav-item active">
                                <a className="nav-link" href="#"> Trang chủ <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#combo"> Combo </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#pizza-type"> Loại pizza </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#send-order"> Gửi đơn hàng </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    {/* Ends Navbar menu */}
    {/* Body */}
    <div className="container">
        <div className="row">
            <div className="col-sm-12">
                <div className="row">
                    {/* Title */}
                    <div className="col-sm-12 text-left">
                        <h1 className = "font-weight-bold web-text">PIZZA 365</h1>
                        <h3 className = "font-italic web-text">Truly italian!!!</h3>
                    </div>

                    {/* Slide */}
                    <div className="col-sm-12">
                        <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                          <ol className="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            
                          </ol>
                          <div className="carousel-inner">
                            <div className="carousel-item active">
                              <img className="d-block w-100" src={img1} alt="First slide"/>
                            </div>
                            <div className="carousel-item">
                              <img className="d-block w-100" src={img2} alt="Second slide"/>
                            </div>
                            <div className="carousel-item">
                              <img className="d-block w-100" src={img3} alt="Third slide"/>
                            </div>
                            <div className="carousel-item">
                              <img className="d-block w-100" src={img4} alt="Fourth slide"/>
                            </div>
                          </div>
                          <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                          </a>
                          <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                          </a>
                        </div>
                      </div>
                    {/* Title Tại sao lại pizza 365 */}
                    <div className="col-sm-12 text-center p-4 mt-4">
                        <h2 className = "web-text"><b className="p-2 border-bottom border-warning">Tại sao lại Pizza 365</b></h2>
                    </div>
                    {/* Content  */}
                    <div className="col-sm-12">
                        <div className="row">
                        <div className="col-sm-3 p-4 border-warning" style = {{backgroundColor: "lightgoldenrodyellow"}}>
                            <h3 className="p-2">Đa dạng</h3>
                            <p className="p-2">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</p>
                        </div>
                        <div className="col-sm-3 p-4 border-warning" style = {{backgroundColor: "yellow"}}>
                            <h3 className="p-2">Chất lượng</h3>
                            <p className="p-2">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                        </div>
                        <div className="col-sm-3 p-4 border-warning" style = {{backgroundColor: "lightsalmon"}}>
                            <h3 className="p-2">Hương vị</h3>
                            <p className="p-2">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</p>
                        </div>
                        <div className="col-sm-3 p-4 border-warning orange">
                            <h3 className="p-2">Dịch vụ</h3>
                            <p className="p-2">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
                        </div>
                        </div>
                    </div>
                    {/* Title Pizza Plans */}
                    <div id="combo" className="row">
                        {/* Title  */}
                        <div className="col-sm-12 text-center p-4 mt-4">
                          <h2 className = "web-text"><b className="p-1 border-bottom border-warning">Chọn size pizza</b></h2>
                          <p><span className="p-2 text-warning">Chọn combo pizza phù hợp với nhu cầu của bạn!</span></p>
                        </div>
                        {/* Plans */}
                        <div className="col-sm-12">
                          <div className="row">
                            <div className="col-sm-4">
                              <div className="card">
                                <div className="card-header text-dark text-center orange">
                                  <h3>S (small)</h3>
                                </div>
                                <div className="card-body text-center">
                                  <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>20cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>2</b></li>
                                    <li className="list-group-item">Salad: <b>200g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>2</b></li>
                                    <li className="list-group-item">
                                      <h1><b>150.000</b></h1> VNĐ
                                    </li>
                                  </ul>
                                </div>
                                <div className="card-footer text-center">
                                  <button className="btn w-100 web-button font-weight-bold" id = "btn-small-click">Chọn</button>
                                </div>
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="card">
                                <div className="card-header text-dark text-center orange">
                                  <h3>M (medium)</h3>
                                </div>
                                <div className="card-body text-center">
                                  <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>25cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>4</b></li>
                                    <li className="list-group-item">Salad: <b>300g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>3</b></li>
                                    <li className="list-group-item">
                                      <h1><b>200.000</b></h1> VNĐ
                                    </li>
                                  </ul>
                                </div>
                                <div className="card-footer text-center">
                                  <button className="btn w-100 web-button font-weight-bold" id = "btn-medium-click">Chọn</button>
                                </div>
                              </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card">
                                  <div className="card-header text-dark text-center orange">
                                    <h3>L (large)</h3>
                                  </div>
                                  <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                      <li className="list-group-item">Đường kính: <b>30cm</b></li>
                                      <li className="list-group-item">Sườn nướng: <b>8</b></li>
                                      <li className="list-group-item">Salad: <b>500g</b></li>
                                      <li className="list-group-item">Nước ngọt: <b>4</b></li>
                                      <li className="list-group-item">
                                        <h1><b>250.000</b></h1> VNĐ
                                      </li>
                                    </ul>
                                  </div>
                                  <div className="card-footer text-center">
                                    <button className="btn w-100 web-button font-weight-bold" id = "btn-large-click">Chọn</button>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                    {/* Title Pizza Type */}
                    <div id="pizza-type" className="row">
                        <div className="col-sm-12 text-center p-4 mt-4">
                          <h2 className = "web-text"><b className="p-2 border-bottom border-warning">Chọn loại Pizza</b></h2>
                        </div>              
                        {/* Content Chọn loại Pizza */}
                        <div className="col-sm-12">
                          <div className="row">
                            <div className="col-sm-4">
                              <div className="card w-100" style={{width: "18rem"}}>
                                <img src={seafoodPizza} className="card-img-top"/>
                                <div className="card-body">
                                  <h3>OCEAN MANIA</h3>
                                  <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                  <p>Xốt Cà Chua, Phô Mai Mozzaella, Tôm, Mực, Thanh Cua, Hành Tây.
                                  </p>
                                  <p><button className="btn w-100 web-button font-weight-bold" id = "btn-chon-seafood">Chọn</button></p>
                                </div>
                              </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card w-100" style={{width: "18rem"}}>
                                  <img src={hawaiiPizza} className="card-img-top"/>
                                  <div className="card-body">
                                    <h3>HAWAIIAN</h3>
                                    <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                    <p>Xốt Cà Chua, Phô Mai Mozzaella, Thịt Dăm Bông, Thơm.
                                    </p>
                                    <p><button className="btn w-100 web-button font-weight-bold" id = "btn-chon-hawaii">Chọn</button></p>
                                  </div>
                                </div>
                              </div>
                              <div className="col-sm-4">
                                <div className="card w-100" style={{width: "18rem"}}>
                                  <img src={baconPizza} className="card-img-top"/>
                                  <div className="card-body">
                                    <h3>CHEESY CHICKEN BACON</h3>
                                    <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                    <p>Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzaella, Cà Chua.
                                    </p>
                                    <p><button className="btn w-100 web-button font-weight-bold" id = "btn-chon-bacon">Chọn</button></p>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>

                      {/* Select a drink */}
                    <div className="col-sm-12 text-center p-4 mt-4">
                        <h2 className = "web-text"><b className = "border-bottom border-warning">Chọn đồ uống</b></h2>        
                    </div>
                      <select id="select-drink-list" className="form-control">
                        <option value = "0"> -- Tất cả loại nước uống -- </option>
                      </select>
                    
                    {/* Title Gửi đơn hàng */}
                    <div id="send-order" >
                    </div>
                      <div className="col-sm-12 text-center p-4 mt-4">
                        <h2 className = "web-text"><b className="p-2 mt-4 border-bottom border-warning">Gửi đơn hàng</b></h2>
                      </div>              
                      {/* Content Input form gửi đơn hàng */}
                      <div className="col-sm-12">
                            <div className="form-group">
                              <label>Tên</label>
                              <input className = "form-control" type = "text" id = "inp-name" placeholder = "Nhập tên"/>
                            </div>
                            <div className="form-group">
                              <label>Email</label>
                              <input className = "form-control" type = "text" id = "inp-email" placeholder = "Nhập email"/>
                            </div>
                            <div className="form-group">
                              <label >Số điện thoại</label>
                              <input className = "form-control" type = "text" id = "inp-phone-number" placeholder = "Nhập số điện thoại"/>
                            </div>
                            <div className="form-group">
                              <label>Địa chỉ</label>
                              <input className = "form-control" type = "text" id = "inp-address" placeholder = "Nhập địa chỉ"/>
                            </div>
                            <div className="form-group">
                              <label>Mã giảm giá</label>
                              <input className = "form-control" type = "text" id = "inp-voucher-id" placeholder = "Nhập mã giảm giá"/>
                            </div>
                            <div className="form-group">
                              <label >Lời nhắn</label>
                              <input className = "form-control" type = "text" id = "inp-message" placeholder = "Nhập lời nhắn"/>
                            </div>
                            <button type="button" className="btn w-100 web-button font-weight-bold" id = "btn-gui-don">Gửi</button>
                      </div>
                   
                   
              </div>
        </div>
    </div>
  </div>
  {/* Footer */}
    <div className="container-fluid p-5 orange">
      <div className="row text-center">
        <div className="col-sm-12">
          <h4 className="m-2 font-weight-bold">Footer</h4>
          <a href="#" className="btn btn-dark m-3"><FaArrowUp/>&nbsp; To the top</a>
          <div className="m-2" style={{fontSize:"20px" }}>
            <FaFacebook/>
            <FaInstagram/>
            <FaSnapchat/>
            <FaPinterest/>
            <FaTwitter/>
            <FaLinkedin/>
          </div>
          <h5 className = "font-weight-bold">Powered by DEVCAMP</h5>
        </div>
      </div>
    </div>

   </div>
  );
}

export default App;
